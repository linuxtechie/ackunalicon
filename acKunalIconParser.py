#!/usr/bin/env python
# encoding: utf-8
'''
acKunalIconParser -- shortdesc

acKunalIconParser is a description

It defines classes_and_methods

@author:     user_name

@copyright:  2015 organization_name. All rights reserved.

@license:    license

@contact:    user_email
@deffield    updated: Updated
'''

import sys
import os
import requests,wget
from lxml import html
import threading
import Queue
from multiprocessing import Lock
import time
import traceback
import csv

__all__ = []
__version__ = 0.1
__date__ = '2015-11-22'
__updated__ = '2015-11-22'

DEBUG = 1
TESTRUN = 0
PROFILE = 0

global workDir
workDir = ""

def rotateFile(fname, i=0):
  if i == 0:
    global workDir
    fname = os.path.join(workDir, fname)
  if not os.path.exists(fname):
    return fname
  while True:
    sfn = fname
    if i > 0:
      sfn = "%s.%d" % (fname, i)
    i = i + 1
    dfn = "%s.%d" % (fname, i)
    if os.path.exists(sfn):
      if os.path.exists(dfn):
        fname = rotateFile(fname, i)
      os.rename(sfn, dfn)
      return fname
    dfn = fname + ".1"
    if os.path.exists(fname):
      os.rename(fname, dfn)
      return fname


class acKunalIcon(threading.Thread):
  def __init__(self, i, session_requests, queue, myLock, logFile, csvf, csvFile, csvLock, url):
    threading.Thread.__init__(self)
    self.url = url
    self.session_requests = session_requests
    self.queue = queue
    self.myLock = myLock
    self.killF = False
    self.id = i
    self.indlog = logFile
    self.csvf = csvf
    self.csvfile = csvFile
    self.csvLock = csvLock

  def appendCsv(self, row):
    if self.killF:
      return
    with self.csvLock:
      self.csvfile.writerow(row)
      self.csvf.flush()

  def download_file(self, url, local_filename):
    # local_filename = url.split('/')[-1]
      # NOTE the stream=True parameter
      if os.path.exists(local_filename):
        return -1
      fs = 0
      r = self.session_requests.get(url, stream=True)
      with open(local_filename, 'wb') as f:
          for chunk in r.iter_content(chunk_size=1024): 
              if chunk: # filter out keep-alive new chunks
                  f.write(chunk)
                  fs = fs + len(chunk)
                  os.fsync(f)
                  #f.flush() commented by recommendation from J.F.Sebastian
      return fs

  def writeLog(self, text):
    with self.csvLock:
      self.indlog.write(text)
      self.indlog.flush()
      os.fsync(self.indlog.fileno())
    
  def run(self):
    self.writeLog("Thread %d Started.\n" % self.id)
    while not self.killF:
      itm = self.queue.get()
      try:
        self.getDocuments(itm)
      except Exception as e:
        self.queue.put(itm)
        self.writeLog("!!!Try Again:  %s\n%s\r\n" % (str(e), e.__doc__))
        traceback.format_exc()
      self.queue.task_done()
    self.writeLog("Thread %d Finished.\n" % self.id)
    
  def getDocuments(self, itm):
    cmdArr = []
    block = itm['block']
    unit_no = itm['unit_no']
    docUrl = itm['url']
    cmdArr.append(block)
    cmdArr.append(unit_no)
    cmdArr.append(docUrl)
    result = self.session_requests.get(
      docUrl, 
      headers = dict(referer = self.url)
    )
    if result.status_code != 200:
      print "Couldn't Fetch file listings"
    else:
      print "Fetched file listings."

    tree = html.fromstring(result.content)
    print "Fetching details for %s - %s" % (block, unit_no)
    docslist = tree.findall(".//*[@id=\"service_staff_docs_list\"]/tbody/tr")
    n = []
    u = []
    for a in docslist:
      fileUrl = a.xpath("td[1]/a")[0].attrib["href"]
      fileName = a.xpath("td[1]/a")[0].text_content()
      n.append(fileName)
      u.append(fileUrl)
      destNDir = os.path.join("OUTPUT",block, unit_no)
      with self.myLock:
        if not os.path.exists(destNDir):
          os.makedirs(destNDir)
      savedDir = os.path.join("OUTPUT", block, unit_no, fileName)
      self.download_file(fileUrl, savedDir)
      print "Saved: %s" % savedDir
    cmdArr.append(';'.join(n))
    cmdArr.append(';'.join(u))
    self.appendCsv(cmdArr)

  def Begin(self):
    pass

def beginProgram():
  logFile = file(rotateFile("Output.log"), "wb+")
  csvf = open(rotateFile("Result.csv"), "wb")
  csvfile = csv.writer(
          csvf,
          delimiter=',',
          quotechar='"',
          doublequote=True,
          skipinitialspace=False,
          lineterminator='\r\n',
          quoting=csv.QUOTE_MINIMAL)
  hdr = []
  hdr.append("Block")
  hdr.append("Unit")
  hdr.append("Main URL")
  hdr.append("FileName")
  hdr.append("FileNameUrl")
  csvfile.writerow(hdr)

  queue = Queue.Queue()
  myLock = Lock()
  csvLock = Lock()

  payload = {
             "email": "veeresh@khanorkar.com",
             "password": "loveena5751"
  }
  session_requests = requests.session()
  login_url = "https://www.apnacomplex.com/auth/login"
  result = session_requests.get(login_url)
  result = session_requests.post(
    login_url, 
    data = payload, 
    headers = dict(referer=login_url)
  )
  if result.status_code == 200:
    print "ApnaComplex Logged On."
  else:
    print "Couldn't Login On ApnaComplex!"
    return

  url = "https://www.apnacomplex.com/runit/ru_directory/"
  result = session_requests.get(
    url, 
    headers = dict(referer = url)
  )
  if result.status_code != 200:
    print "Couldn't Fetch directory"
    return
  
  print "Fetched directory."
#     self.getDocuments("B2", "403", "https://www.apnacomplex.com/runit/ru_manage_document/eNortjKzUjIytDAws1CyBlwwFYoC1g~~")
  DEBUG = False
  if not DEBUG:  
    tree = html.fromstring(result.content)
    bucket_elems = tree.findall(".//*[@id=\"ru_list\"]/tbody/tr")
    for bucket in bucket_elems:
      block = bucket.xpath("td[3]")[0].text_content()
      unit_no =  bucket.xpath("td[4]")[0].text_content()
      docs = bucket.xpath("td[2]/a[3]")[0].attrib['href'] 
      print "Fetching details for : %s - %s" % (block, unit_no)
      dt = {
            "block" : block,
            "unit_no": unit_no,
            "url": docs
            }
      queue.put(dt)
  else:
    dt = {
            "block" : "B2",
            "unit_no": "401",
            "url": "https://www.apnacomplex.com/runit/ru_manage_document/eNortjKzUjIytDAws1CyBlwwFYoC1g~~"
    }
    queue.put(dt)
    dt = {
            "block" : "B2",
            "unit_no": "402",
            "url": "https://www.apnacomplex.com/runit/ru_manage_document/eNortjKzUjIytDAws1CyBlwwFYoC1g~~"
    }
    queue.put(dt)
    dt = {
            "block" : "B2",
            "unit_no": "403",
            "url": "https://www.apnacomplex.com/runit/ru_manage_document/eNortjKzUjIytDAws1CyBlwwFYoC1g~~"
    }
    queue.put(dt)


  threads = []
  for i in range(20):  # @UnusedVariable
    t = acKunalIcon(i, session_requests, queue, myLock, logFile, csvf, csvfile, csvLock, url)
    t.setDaemon(True)
    t.start()
    threads.append(t)
      
  try:
      while True:
        time.sleep(0.5)
        print "Waiting For Threads To Finish..."
        if queue.empty() is True:
          break
  except KeyboardInterrupt:
    print "[X] Interrupt! Killing threads..."
    # Substitute the old queue with a new empty one and exit loop
    for t in threads:
      t.killF = True
    queue = Queue.Queue()
  print "Waiting Finished"
  try:
    queue.join()
  except:
#       print "No immediate kill, since all the threads had taken "
    pass

  print "All done complete."

if __name__ == "__main__":
  beginProgram()
